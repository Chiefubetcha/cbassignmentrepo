package exceptions;

public class ShapeException extends SystemException {
	
	public ShapeException(final String name) throws SystemException {
		super(name + " was not found. Please make sure it was spelled correctly.");
		
	}
}