package com.shape.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.shape.Shape;


public class StorageDB {

	static Scanner sc = new Scanner(System.in);

	private static Connection conn = null;
	private static Statement stmt = null;
	private ResultSet rs = null;
	private int id;
	private String name;
	private String colour;
	private int quantity;

	public static void main(String[] args) {
		initiate_db_conn();
	}

	public static Connection initiate_db_conn()
	{
		try
		{
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			String url="jdbc:mysql://localhost:3306/shapes?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

			
			conn = DriverManager.getConnection(url, "root", "admin");
			stmt = conn.createStatement();
		}
		catch(Exception e)
		{
			System.out.println("Error: Failed to connect to database\n"+e.getMessage());
		}
		
		return conn;
	}

	public Shape actionSearchByName(String str) throws SQLException {

		initiate_db_conn();

		Shape shape = null;

		String result = "";
		String cmd;

		cmd = "SELECT * FROM shape WHERE name =  '" + str + "';";
		ResultSetMetaData rsmd = null;


		rs = stmt.executeQuery(cmd);

		while(rs.next()) {
			int id = rs.getInt("id");
			String name = rs.getString("name");
			String colour = rs.getString("colour");
			int quantity = rs.getInt("quantity");
			shape = new Shape(id, name, colour, quantity);
			
				
		}
		return shape;
	}


	public String actionRemoveFromShapeList(String x) {

		initiate_db_conn();


		String cmd;

		cmd = "DELETE FROM shape WHERE name =  '" + x + "';";
		ResultSetMetaData rsmd = null;
		try {
			stmt.executeUpdate(cmd);
		} catch (SQLException e){
			System.out.println("An SQL error occurred...");
		}
		String result = "Performing task...";

		return(result);

	}



	public void actionAddToStorageList(Shape shape) throws SQLException {

		initiate_db_conn();

		String result = "Shape added to list.";
		String cmd;


		cmd = "INSERT INTO shape VALUES ("+ shape.getId() +", '"+shape.getName()+"', '"+shape.getColour()+"', "+shape.getQuantity()+");";
		ResultSetMetaData rsmd = null;
		try {
			stmt.executeUpdate(cmd);
		} catch (SQLException e){
			e.printStackTrace(); 
		}
		System.out.println(result);

	}

	public int actionUpdate(String name, int quantity) throws SQLException {
		//placeholder
		initiate_db_conn();

		String result = "Shape updated.";
		String cmd;

		cmd = "update shape set quantity = " + quantity + " where name = '" + name + "';";

		ResultSetMetaData rsmd = null;
		try {
			stmt.executeUpdate(cmd);
			System.out.println("The quantity of " + name + " in stock is now " + quantity);
		} catch (SQLException e){
			e.printStackTrace(); 
		}
		return quantity;
	}

	public List<String> actionViewStorageList() throws SQLException {
		
		List<String> shapeList = new ArrayList<String>();

		initiate_db_conn();

		String result = "";
		String cmd;

		cmd = "SELECT * FROM shape;";
		ResultSetMetaData rsmd = null;
		try {
			rs = stmt.executeQuery(cmd);
		} catch (SQLException e){
			System.out.println("An SQL error occurred...");
			return null;
		}

		while(rs.next()) {
			result = "\nID: " + rs.getObject("id") +"\nName: " + rs.getObject("name") +
					"\nColour: " + rs.getObject("colour") + 
					"\nQuantity in stock: " + rs.getObject("quantity") + "\n";
			if(result != null) {

				shapeList.add(result);
	
			}

		}

		return shapeList;

	}


}
