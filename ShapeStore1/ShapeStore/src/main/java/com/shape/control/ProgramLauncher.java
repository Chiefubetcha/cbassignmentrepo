package com.shape.control;

import java.util.Scanner;


public class ProgramLauncher {

	static Scanner sc = new Scanner(System.in);
	private static ShapeStoreControl ssc = new ShapeStoreControl();
	
	public static void main(String[] args) throws Exception {
		welcome();
	}

	public static void welcome() throws Exception {
		System.out.println("");
		String message = "Welcome to the ShapeStore App!";
		System.out.println(message);	
		
		selector(options());
	}

	public static int options() throws Exception {

		System.out.println("Please choose an option: ");
		System.out.println("");
		System.out.println("Search by name: Press 1");
		System.out.println("Update quantity: Press 2");
		System.out.println("Add new shape: Press 3");
		System.out.println("Remove shape: Press 4");
		System.out.println("Show shape list: Press 5");
		System.out.println("");
		System.out.println("To exit: Press 6");

		int choice = sc.nextInt();

		return choice;
		
	}
	
	public static String inputName() {
		System.out.println("Please enter a shape: ");
		String entry = sc.next();
		return entry;
	}
	
	public static String inputColour() {
		System.out.println("Please enter a colour: ");
		String entry = sc.next();
		return entry;
	}
	
	public static int inputNumber() {
		System.out.println("Please enter the quantity: ");
		int num = sc.nextInt();
		return num;
	}
	
	public static int inputId() {
		System.out.println("Please enter an id: ");
		int num = sc.nextInt();
		return num;
	}

	public static String selector(int a) throws Exception {

		String tag = "";
		String name;
		String colour;
		int quantity;
		int id;
		
		if(a == 1) {
			name = inputName();			
			tag = "search";	
			ssc.shapeSearch(name);
		}else if(a == 2) {
			System.out.println("Shape to be updated");
			name = inputName();
			quantity = inputNumber();
			tag = "update";
			ssc.updateShape(name, quantity);
		}else if(a == 3) {
			name = inputName();
			colour = inputColour();		
			quantity = inputNumber();
			id = inputId();			
			ssc.addShape(name, colour, quantity, id);
		}else if(a == 4) {
			name = inputName();
			tag = "remove";
			ssc.deleteShape(name);
		}else if(a == 5) {
			tag = "list";
			ssc.showStorageList();
		}else if(a == 6) {
			tag = "exit";
			System.out.println("Thank you for using ShapeStore! Goodbye.");
		}else {
			tag = "invalid";
			System.out.println("Invalid entry.");
			selector(options());
		}
		
		return tag;
	}

}
