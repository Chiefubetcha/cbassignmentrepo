package com.shape;


public class Shape {

		private int id;
		private String name;
		private String colour;
		private int quantity;

		public Shape(int id, String name, String colour, int quantity) {
			this.id = id;
			this.name = name;
			this.colour = colour;
			this.quantity = quantity;
		}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getColour() {
		return colour;
	}

	public void setColour(String colour) {
		this.colour = colour;
	}
	
	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	@Override
	public String toString() {
		return   "ID: " + id +" \nName: " + name +
				" \nColour: " + colour +
				" \nQuantity stored: " + quantity;
	}

}
