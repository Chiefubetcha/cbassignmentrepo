package com.test.shape;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.shape.Shape;

class ShapeUTest {

	Shape shape;

	private final String name = "circle";
	private final String colour = "blue";
	private final int id = 99;
	private int quantity = 4;

	String test;
	int identifier;

	@BeforeEach
	public void setup() {
		shape = new Shape(id, name, colour, quantity);

	}

	@Test
	public void testGetId() {
		identifier = shape.getId();
		assertEquals(99, identifier);
	}

	@Test
	public void testSetId() {
		shape.setId(2);
		identifier = shape.getId();
		assertEquals(2, identifier);
	}

	@Test
	public void testGetShapeName() {
		test = shape.getName();
		assertEquals("circle", test);
	}

	@Test
	public void testSetshapeName() {
		shape.setName("square");
		test = shape.getName();
		assertEquals("square", test);
	}

	@Test
	public void testGetShapeColour() {
		test = shape.getColour();
		assertEquals("blue", test);
	}

	@Test
	public void testSetShapeColour() {
		shape.setColour("red");
		test = shape.getColour();
		assertEquals("red", test);
	}

	@Test
	public void testSetQuantity() {
		shape.setQuantity(6);
		identifier = shape.getQuantity();
		assertEquals(identifier, 6);
	}

	@Test
	public void testGetQuantity() {
		identifier = shape.getQuantity();
		assertEquals(identifier, 4);
	}

	@Test
	public void testToString() {

		String details = shape.toString();
		assertEquals("ID: 99 \nName: circle \nColour: blue \nQuantity stored: 4", details);

	}

}
