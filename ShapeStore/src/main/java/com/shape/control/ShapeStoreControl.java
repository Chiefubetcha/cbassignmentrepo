package com.shape.control;

import java.util.Scanner;

import com.shape.*;
import com.shape.dao.StorageDB;
import exceptions.SystemException;
//import messages.Results;
import exceptions.ShapeException;

import com.shape.control.ProgramLauncher;


public class ShapeStoreControl {

	static Scanner sc = new Scanner(System.in);
	private ProgramLauncher launch = new ProgramLauncher();
	private StorageDB stock = new StorageDB();
//	private Results res = new Results();

	


	public void shapeSearch(String name) throws Exception{


		final Shape result = (stock.actionSearchByName(name));
		if(result != null) {
			String str = result.toString();
			System.out.println(str);
			

		}else {
			try {
				throw new ShapeException(name);
			}catch(ShapeException e) {
				System.out.println("Caught ShapeException: shape could not be found");
			}
		}
		launch.selector(launch.options());
	}

	public void updateShape(String name, int quantity) throws Exception, SystemException{

		stock = new StorageDB();
		
		Shape result = stock.actionSearchByName(name);
		System.out.println("");
		if(result != null) {
			stock.actionUpdate(name, quantity);
//			sc.nextLine();
		} else {
			try {
				throw new ShapeException(name);
			}catch(ShapeException e) {
				System.out.println("Caught ShapeException: shape could not be found");
		}
			
		}
		launch.selector(launch.options());
	}

	public void deleteShape(String name) throws Exception, SystemException{

		stock = new StorageDB();
		System.out.println("Shape removed");
		Shape result = stock.actionSearchByName(name);
		System.out.println("");
		if(result != null) {

			stock.actionRemoveFromShapeList(name);
			String str = (name + " has been removed from the list");

			System.out.println("");

		} else {
			try {
				throw new ShapeException(name);
			}catch(ShapeException e) {
				System.out.println("Caught ShapeException: shape could not be found");
			}
		}

		launch.selector(launch.options());
	}

	public void addShape(String name, String colour, int quantity, int id) throws Exception {

		stock = new StorageDB();

		final Shape result = (stock.actionSearchByName(name));
		if(result != null) {
			System.out.println("This shape already exists.");
			launch.selector(launch.options());
		}

		else {

			Shape shape = new Shape(0, null, null, 0);

			if(name != null) 
				shape.setName(name);

			if(colour != null)
				shape.setColour(colour);

			if(quantity != 0)
				shape.setQuantity(quantity);

			if(id != 0)
				shape.setId(id);

			try {

				stock.actionAddToStorageList(shape);
			}
			catch (Exception e) {
				System.out.println("Caught Exception");
				System.out.println("Original exception: " + e.getCause().toString());
			}
		}

		launch.selector(launch.options());

	}

	public void showStorageList() throws Exception {

		stock = new StorageDB();


		System.out.println(stock.actionViewStorageList());
		System.out.println("");
		launch.selector(launch.options());

	}


}


