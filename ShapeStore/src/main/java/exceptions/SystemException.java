package exceptions;

public abstract class SystemException extends Exception {

	protected SystemException(final String message) {
		super(message);
	}
}
