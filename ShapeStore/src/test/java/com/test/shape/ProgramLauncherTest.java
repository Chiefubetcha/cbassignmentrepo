package com.test.shape;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import java.io.ByteArrayOutputStream;
import java.sql.SQLException;

import static org.mockito.Mockito.anyDouble;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.internal.verification.Times;

import com.shape.Shape;
import com.shape.control.ProgramLauncher;
import com.shape.control.ShapeStoreControl;
import com.shape.dao.StorageDB;

class ProgramLauncherTest {
	
	Shape shape;

	private final String name = "heptagon";
	private final String colour = "silver";
	private final int id = 50;
	private final int quantity = 1;
	
	private final ShapeStoreControl control = mock(ShapeStoreControl.class);
	private final StorageDB db = mock(StorageDB.class);
	//private ProgramLauncher launch = mock(ProgramLauncher.class);
	
	
//	private ShapeStoreControl control;
	
	@BeforeEach
	public void setup() {
		shape = new Shape(id, name, colour, quantity);
		shape.setId(id);
		shape.setName(name);
		shape.setColour(colour);
		shape.setQuantity(quantity);
//		control = new ShapeStoreControl();

	}
	
	@Test
	public void testSearchShape() throws Exception {

		when(db.actionSearchByName(name)).thenReturn(shape);
		Shape result = db.actionSearchByName(name);
		assertEquals("silver", result.getColour());
	}

	
	
	
}
