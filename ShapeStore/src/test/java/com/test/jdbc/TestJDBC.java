package com.test.jdbc;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.Connection;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import com.shape.dao.StorageDB;

class TestJDBC {
	
	StorageDB db = new StorageDB();

	@Test
	public void testOpenConnection() {
		Connection result = db.initiate_db_conn();
		assertEquals(result != null, true);
	}
	
	
}
